import {Component, OnInit} from '@angular/core';
import {PhotoService} from '../photo.service';

@Component({
  selector: 'app-photo-list',
  templateUrl: './photo-list.component.html',
  styleUrls: ['./photo-list.component.scss']
})
export class PhotoListComponent implements OnInit {
  photos = this.photoService.photos;
  canLoadMore = this.photoService.canLoadMore;

  constructor(private photoService: PhotoService) {
  }

  ngOnInit() {
  }

  loadMore() {
    this.photoService.loadMore().subscribe({error: () => alert('Nie udało się pobrać zdjęć!')});
  }
}

import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BlackHoleComponent} from './black-hole/black-hole.component';
import {DetectorComponent} from './planet-detector/detector/detector.component';
import {PhotoBrowserComponent} from './photo/photo-browser/photo-browser.component';


const routes: Routes = [
  {path: '', redirectTo: 'space', pathMatch: 'full'},
  {path: 'planet-detector', component: DetectorComponent},
  {path: 'photo-browser', component: PhotoBrowserComponent},
  {path: 'intel-browser', loadChildren: () => import('./intel/intel.module').then(m => m.IntelModule)},
  {path: '**', component: BlackHoleComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

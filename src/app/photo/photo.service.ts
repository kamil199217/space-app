import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {Photo, PhotoCredentials, PhotoResponse} from './data.types';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  private queryParams = {
    user_id: 'kamil.adamuszek@gmail.com',
    api_key: '3047411f6eab2044932c7edc6d1f9de5',
    page: '1',
    per_page: '10',
    extras: 'url_l',
    method: 'flickr.people.getPhotos',
    format: 'json',
    nojsoncallback: '1'
  };

  photos = new BehaviorSubject<Photo[]>([]);
  canLoadMore = this.photos.pipe(map((photos) => photos.length > 0));

  constructor(private http: HttpClient) {
  }

  fetchPhotos(crendetials: PhotoCredentials): Observable<Photo[]> {
    this.queryParams.user_id = crendetials.userId;
    this.queryParams.api_key = crendetials.apiKey;
    return this.loadPhotos();
  }

  loadMore(): Observable<Photo[]> {
    this.queryParams.page = (parseInt(this.queryParams.page, 10) + 1).toString();
    return this.loadPhotos();
  }

  private loadPhotos(): Observable<Photo[]> {
    return this.http.get<PhotoResponse>('https://api.flickr.com/services/rest', {params: this.queryParams}).pipe(
      map((photoResponse) => photoResponse.photos.photo.filter((photo) => !!photo.url_l)),
      tap((photos) => this.photos.next([...this.photos.getValue(), ...photos]))
    );
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanetDetectorRoutingModule } from './planet-detector-routing.module';
import { DetectorComponent } from './detector/detector.component';


@NgModule({
  declarations: [DetectorComponent],
  imports: [
    CommonModule,
    PlanetDetectorRoutingModule
  ],
  exports: [
    DetectorComponent
  ]
})
export class PlanetDetectorModule { }

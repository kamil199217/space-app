import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Position} from '../position';
const SightDistance = 100;
const DetectDistance = 15;
@Component({
  selector: 'app-detector',
  templateUrl: './detector.component.html',
  styleUrls: ['./detector.component.scss']
})
export class DetectorComponent implements OnInit {
  @ViewChild('game', {static: true}) gameElement: ElementRef<HTMLDivElement>;
  isDetected = false;
  currentPosition: Position = {x: 0, y: 0};
  planetPosition: Position;
  noiseOpacity = 1;
  private distance = SightDistance;
  constructor() {
  }

  ngOnInit() {
    this.setup();
  }

  updatePosition(position: Position) {
    this.currentPosition = position;
    this.distance = this.getDistance();
    this.noiseOpacity = this.getNoiseOpacity();
  }

  detect() {
    if (this.distance < DetectDistance) { this.isDetected = true; }
  }

  getDistance() {
    const dx = this.currentPosition.x - this.planetPosition.x;
    const dy = this.currentPosition.y - this.planetPosition.y;
    return Math.sqrt(dx * dx + dy * dy);
  }
  private getNoiseOpacity(): number {
    if (this.isDetected) { return 0; }
    return Math.min(1, this.distance / SightDistance);
  }

  private setup() {
    const dimensions = this.gameElement.nativeElement.getBoundingClientRect();
    this.planetPosition = {
      x: Math.floor(Math.random() * dimensions.width),
      y: Math.floor(Math.random() * dimensions.height)
    };
  }

}

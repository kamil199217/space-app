import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhotoBrowserComponent } from './photo-browser/photo-browser.component';
import { PhotoFormComponent } from './photo-form/photo-form.component';
import { PhotoListComponent } from './photo-list/photo-list.component';
import {FormsModule} from '@angular/forms';
import {PhotoRoutingModule} from './photo-routing.module';



@NgModule({
  declarations: [PhotoBrowserComponent, PhotoFormComponent, PhotoListComponent],
  imports: [
    CommonModule,
    FormsModule,
    PhotoRoutingModule
  ]
})
export class PhotoModule { }
